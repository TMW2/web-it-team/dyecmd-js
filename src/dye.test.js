const assert = require('assert')
const { join } = require('path')
const { readFile, writeFile } = require('fs-extra')
const { decode, encode } = require('fast-png')

const dye = require('./dye')

async function getImgData(name) {
    const buffer = await readFile(join(__dirname, '../test_assets/', `${name}.png`))
    return decode(buffer)
}

describe('Dye Image', function () {
    const tests = [
        { expectedResultImage: "1", dyeString: "S:#000000,#00FF00;" },
        //{ expectedResultImage: "2", dyeString: "S:#00ff00ff,00ff00ff" },
        { expectedResultImage: "3", dyeString: "S:#ffffff,005Ff0;" },
        { expectedResultImage: "4", dyeString: "W:#640088,b35000,ecb7ff;" },
        { expectedResultImage: "5", dyeString: "R:#c00034,01ea8e,f07faa" },
        { expectedResultImage: "6", dyeString: "G:#ccb534,f1ea8e,ffffaa" },
        //{ expectedResultImage: "7", dyeString: "B:#ccb534,f1ea8e,ffffaa,c00034,640088;" },
        { expectedResultImage: "8", dyeString: "C:#44af08,b3ff00,c00034" },
        { expectedResultImage: "9", dyeString: "M:#a4a088,b3ff00,44af08,44af08" },
        { expectedResultImage: "a", dyeString: "Y:#64f088,b3ff00,c00034" },
        { expectedResultImage: "b", dyeString: "W:#640088,b35000,ecb7ff;Y:#ffdf00,b3dd00,ddb7Df;" },
        //{ expectedResultImage: "c", dyeString: "Y:#ffdf00,b3dd00,ddb7df;S:#ffffff,00ff00;" },
        //{ expectedResultImage: "d", dyeString: "W:#343434,ababab;R:#003300,00FF00;G:#29758A,9BDBEC;" },
    ]
    tests.forEach(function (test) {
        it(`${test.expectedResultImage} ${test.dyeString}`, async function () {
            const img_data = await getImgData('TestCase')
            const actual_pixels = dye(img_data.data, test.dyeString)
            const expected_pixels = [...(await getImgData(test.expectedResultImage)).data]
            let equal = true;
            for (let i = 0; i < actual_pixels.length; i++) {
                if (actual_pixels[i] != expected_pixels[i]) {
                    equal = false
                    // console.log(i, actual_pixels[i], expected_pixels[i])
                }
            }
            if (!equal) {
                await writeFile(
                    join(__dirname, '../test_assets/', `${test.expectedResultImage}.a.res.png`),
                    encode({ ...img_data, data: actual_pixels })
                )
                await writeFile(
                    join(__dirname, '../test_assets/', `${test.expectedResultImage}.e.res.png`),
                    encode({ ...img_data, data: expected_pixels })
                )
            }
            assert.equal(equal, true, "Pixels aren't matching")
            // assert.deepStrictEqual(actual_pixels, expected_pixels, "notEqual")
        })
    })
})
