const { join } = require('path')
const { readFile, writeFile } = require('fs-extra')

const { decode, encode } = require('fast-png')

const dye = require('../src/dye')

const fs = require('fs')

async function start() {
    const buffer = await readFile(join(__dirname, '../test_assets/662.png'))
    const imagedata = decode(buffer)

    const dyeString =
        // 'S:#000000,#00FF00;'
        // 'S:#000000ff,#00FF00ff;'
        // 'W:#640088,b35000,ecb7ff'
        // 'W:#640088,b35000,ecb7ff;Y:#ffdf00,b3dd00,ddb7Df'
        'G:#ccb534,f1ea8e,ffffaa'

    const newImageData = dye(imagedata.data, dyeString)

    const newFileBuffer = encode({ ...imagedata, data: newImageData })
    await promisify(writeFile)(join(__dirname, '../test_assets/new.png'), newFileBuffer)
}

start()
