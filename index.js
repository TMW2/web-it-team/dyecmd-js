const parseDyeString = require('./src/parser')
const dye = require('./src/dye')

const { decode, encode } = require('fast-png')
/**
 * 
 * @param {Uint8Array} buffer png file binary content
 * @param {string} dyeString 
 */
function dyePNG(buffer, dyeString) {
    const imagedata = decode(buffer)
    return encode({ ...imagedata, data: dye(imagedata.data, dyeString) })
}

module.exports = {
    parseDyeString,
    dye,
    dyePNG,
}