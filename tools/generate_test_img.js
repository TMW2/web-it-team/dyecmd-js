const { join } = require('path')
const { readFile, writeFile } = require('fs')
const { promisify } = require('util')

const { decode, encode } = require('fast-png')

const fs = require('fs')

async function start() {

    let pixels = []

    for (let i = 0; i < 256; i++) {
        pixels.push(i, i, i, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(i, 0, 0, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(0, i, 0, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(0, 0, i, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(i, i, 0, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(0, i, i, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(i, 0, i, 255)
    }

    for (let i = 0; i < 256; i++) {
        pixels.push(0, 0, 0, 0)
    }


    const newFileBuffer = encode({
        width: 256,
        height: 8,
        data: pixels,
        depth: 8,
        channels: 4,
    })
    await promisify(writeFile)(join(__dirname, '../test_assets/TestCase.png'), newFileBuffer)
}

start()
