[Project goals]


[how to use it]


TODO:

- [X] Parser for dyestring
    - [X] add support for `S:#3c3c3c,35313d,4d4d4d,443f4f,686868,485c6b,919191,50918f,b6b6b6,75c7a8,dfdfdf,c3e8c4` syntax after finding out how it works and documenting it below.
- [X] Image dying
- [-] Tests for Image dying
    - [X] create testcases (dyestrings that get applied on TestCase) then generate from each the correct solution using dyecmd to compare results
    - important test all methods and channels
    - Fix remaining failing tests (commented out ones in `testcases`)
- [X] add index.js that exports all functions that should be accessible when using this as a package
- [ ] write readme
    - usage Example

Ideas:
- [ ] Recreate cli for fun (does it then need to check if run globaly? or is it better to make seperate package for a cli?)
    - if fs-extra isn't used in the main package move it to dev dependencies

- [ ] Create a small webapp to play around with dyestrings?


> Make sure the image you want to dye has a transparency channel


## How dyestrings work

This information was gathered from https://wiki.themanaworld.org/index.php/Dev:Image_dyeing

### RGBCMYW
intensity aware recoloring of colors with a pallete more on https://wiki.themanaworld.org/index.php/Dev:Image_dyeing#Simple_colors_and_palettes

Channel | Trigger Condition | Value
--------|-------------------|-------
**R***ed* | `g` = `b` = 0 | `r`
**G***reen* | `r` = `b` = 0 | `g`
**B***lue* | `r` = `g` = 0 | `b`
**C***yan* | `r` = 0 & `g` = `b` | `g` or `b`
**M***agenta* | `g` = 0 & `r` = `b` | `r` or `b`
**Y***ellow* | `b` = 0 & `r` = `g` | `r` or `g`
**W***hite* (grey) | `r` = `g` = `b` | `r` or `b` or `g`

```
#ColorSequence - comma separated hex notated colors
#Channel - R | G | B | C | M | Y | W
#Channel:#ColorSequence
```

Example:
```
W:#640088,b350e0,ecb7ff
```

> Important: use the `#` only on the first color in the sequence otherwise manaplus will do strange stuff. (use other colors than you specified)

### Swap dye
This swaps a single specific color on a channel regardless of transparency for another 

```
S:#StartingColor,#ResultColor
```

Example:
```
image.png|S:#FF0000,#00FF00; 
```

### Alpha dye
Same as Swap dye but takes transparency into account

```
S:#StartingColor,#ResultColor
```

Example:
```
image.png|S:#FF000088,#00FF0088;
```

### Swap and Alpha Dye - **multiple** in one

You can also write:
```
S:#StartingColor1,#ResultColor1,#StartingColor2,#ResultColor2,#StartingColor3,#ResultColor3
```
Which behaves like:
```
S:#StartingColor1,#ResultColor1;S:#StartingColor2,#ResultColor2;S:#StartingColor3,#ResultColor3
```

Example: 
```
S:#3c3c3c,35313d,4d4d4d,443f4f,686868,485c6b,919191,50918f,b6b6b6,75c7a8,dfdfdf,c3e8c4
```

### See also

- https://wiki.themanaworld.org/index.php/Dev:Image_dyeing
- https://github.com/themanaworld/manaportal