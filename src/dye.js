const parseDyeString = require('./parser')
const parseColor = require('./color')
/**
 * 
 * @param {number[]} pixeldata 
 * @param {string} dyeString 
 */
function dye(pixeldata, dyeString) {
    const steps = parseDyeString(dyeString)

    let workingPixeldata = [...pixeldata];

    for (let i = 0; i < steps.length; i++) {
        const step = steps[i];
        if (step.method === "Recolor") {
            dyeSwapReColor(
                workingPixeldata,
                step.channel,
                step.colorSequence.map(parseColor)
            )
        } else if (step.method === "Swap") {
            dyeSwap(
                workingPixeldata,
                parseColor(step.startColor),
                parseColor(step.resultColor)
            )
        } else if (step.method === "SwapWithAlpha") {
            dyeAlphaSwap(
                workingPixeldata,
                parseColor(step.startColor),
                parseColor(step.resultColor)
            )
        } else {
            throw new Error("Unknown function")
        }
    }

    return workingPixeldata
}

function dyeSwap(pixeldata, start, result) {
    for (let i = 0; i < pixeldata.length; i = i + 4) {
        if (
            start.r === pixeldata[i]
            && start.g === pixeldata[i + 1]
            && start.b === pixeldata[i + 2]
        ) {
            pixeldata[i] = result.r;
            pixeldata[i + 1] = result.g;
            pixeldata[i + 2] = result.b;
        }
    }
}

function dyeAlphaSwap(pixeldata, start, result) {
    for (let i = 0; i < pixeldata.length; i = i + 4) {
        if (
            start.r === pixeldata[i]
            && start.g === pixeldata[i + 1]
            && start.b === pixeldata[i + 2]
            && start.a === pixeldata[i + 3]
        ) {
            pixeldata[i] = result.r;
            pixeldata[i + 1] = result.g;
            pixeldata[i + 2] = result.b;
            pixeldata[i + 3] = result.a;
        }
    }
}

const ReColorModes = {
    R: { trigger: (r, g, b) => g == 0 && b == 0, value: (r, g, b) => r },
    G: { trigger: (r, g, b) => r == 0 && b == 0, value: (r, g, b) => g },
    B: { trigger: (r, g, b) => g == 0 && r == 0, value: (r, g, b) => g },
    C: { trigger: (r, g, b) => r == 0 && g == b, value: (r, g, b) => b },
    M: { trigger: (r, g, b) => g == 0 && r == b, value: (r, g, b) => b },
    Y: { trigger: (r, g, b) => b == 0 && r == g, value: (r, g, b) => g },
    W: { trigger: (r, g, b) => r == g && g == b, value: (r, g, b) => r },
}

function DoReColor(intensity, colorPalette) {
    if (intensity == 0) return [0, 0, 0];

    let j = 0;
    while (intensity > colorPalette[j].p) { j++; }

    if (colorPalette[j].p == intensity) {
        const c = colorPalette[j].c
        return [c.r, c.g, c.b]
    }
    const factor = (intensity - colorPalette[j - 1].p) / (colorPalette[j].p - colorPalette[j - 1].p);
    const a = colorPalette[j - 1].c;
    const b = colorPalette[j].c;

    return [
        Math.floor(a.r + (b.r - a.r) * factor),
        Math.floor(a.g + (b.g - a.g) * factor),
        Math.floor(a.b + (b.b - a.b) * factor),
    ];
}

function dyeSwapReColor(pixeldata, channel, pallete) {
    const mode = ReColorModes[channel]


    const colorPalette = [
        { r: 0, g: 0, b: 0 },
        ...pallete
    ].map((color, index, array) => {
        return {
            p: (255 / (array.length - 1)) * index,
            c: color,
        }
    });

    for (let i = 0; i < pixeldata.length; i = i + 4) {
        const color = pixeldata.slice(i, i + 3)
        if (mode.trigger(...color)) {
            const intensity = mode.value(...color)
            if (intensity == 0) continue;
            const newColor = DoReColor(intensity, colorPalette)

            if (Number.isNaN(newColor[0])) throw new Error("This error should never accur, please contact the developers")

            pixeldata[i] = newColor[0]
            pixeldata[i + 1] = newColor[1]
            pixeldata[i + 2] = newColor[2]
        }

    }
}



module.exports = dye