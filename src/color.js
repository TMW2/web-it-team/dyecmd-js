function ParseColor(hexString) {
    const res = /#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})?/i.exec(hexString)

    let result = {
        r: parseInt(res[1], 16),
        g: parseInt(res[2], 16),
        b: parseInt(res[3], 16),
    }

    if (typeof res[4] != "undefined")
        result.a = parseInt(res[4], 16)

    return result
}


module.exports = ParseColor