const { join } = require('path')
const { readFile, writeFile } = require('fs')
const { promisify } = require('util')

const { decode, encode } = require('fast-png')

const fs = require('fs')

async function start() {

    let pixels = []

    for (let i = 0; i < 32 * 32; i++) {
        pixels.push(i % 255, i % 55 + 50, i % 200 + 30, i % 150 > 100 ? 0 : 255)
    }

    const newFileBuffer = encode({
        width: 32,
        height: 32,
        data: pixels,
        depth: 8,
        channels: 4,
    })
    await promisify(writeFile)(join(__dirname, '../test_assets/TestCase2.png'), newFileBuffer)
}

start()
