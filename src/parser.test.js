const parseDyeString = require('./parser')
var assert = require('assert')

describe('Parser', function () {
    it('RGBCMYW recolor using pallete', function () {
        assert.deepEqual(parseDyeString('W:#640088,b350e0,ecb7ff'), [{
            method: "Recolor",
            channel: "W",
            colorSequence: ['#640088', 'b350e0', 'ecb7ff']
        }])
        assert.deepEqual(parseDyeString('G:#640088,b350e0,ecb7ff;'), [{
            method: "Recolor",
            channel: "G",
            colorSequence: ['#640088', 'b350e0', 'ecb7ff']
        }])
    })
    it('Swap single color', function () {
        assert.deepEqual(parseDyeString('S:#FF0000,#00FF00;'), [{
            method: "Swap",
            startColor: '#FF0000',
            resultColor: '#00FF00'
        }])
    })
    it('Swap single color [multiple in one]', function () {
        assert.deepEqual(parseDyeString('S:#3c3c3c,35313d,4d4d4d,443f4f'), [{
            method: "Swap",
            startColor: '#3c3c3c',
            resultColor: '35313d'
        },{
            method: "Swap",
            startColor: '4d4d4d',
            resultColor: '443f4f'
        }])
    })
    it('Swap single color (colors with alpha channel)', function () {
        assert.deepEqual(parseDyeString('S:#FF000088,#00FF0088;'), [{
            method: "SwapWithAlpha",
            startColor: '#FF000088',
            resultColor: '#00FF0088'
        }])
    })
    it('Swap single color (colors with alpha channel) [multiple in one]', function () {
        assert.deepEqual(parseDyeString('S:#3c3c3cff,35313dff,4d4d4dff,443f4fff'), [{
            method: "SwapWithAlpha",
            startColor: '#3c3c3cff',
            resultColor: '35313dff'
        },{
            method: "SwapWithAlpha",
            startColor: '4d4d4dff',
            resultColor: '443f4fff'
        }])
    })

    it('Multiple parts', function () {
        assert.deepEqual(parseDyeString('S:#FF000088,#00FF0088;W:#640088,b350e0,ecb7ff;'), [{
            method: "SwapWithAlpha",
            startColor: '#FF000088',
            resultColor: '#00FF0088'
        }, {
            method: "Recolor",
            channel: "W",
            colorSequence: ['#640088', 'b350e0', 'ecb7ff']
        }])
    })

    it('Should Fail on malformed dystring', function () {
        // unknown method
        assert.throws(_ => parseDyeString('F:#444444,#445553'))
        // malformed (':' missing)
        assert.throws(_ => parseDyeString('F#444444,#445553'))
    })

    it('Should Fail on malformed Swap (alpha) [multiple in one]', function () {
        assert.throws(_ => parseDyeString('S:#3c3c3cff,35313dff,4d4d4dff'))
        assert.throws(_ => parseDyeString('S:#3c3c3cff,35313dff,4d4d4dff,'))
        assert.throws(_ => parseDyeString('S:#3c3c3cff,,35313dff,4d4d4dff,'))
        assert.throws(_ => parseDyeString('S:#3cc3cff,,35313df,4d4d4dff,'))
        assert.throws(_ => parseDyeString('S:#3cc3cf,35313df'))
    })
})
