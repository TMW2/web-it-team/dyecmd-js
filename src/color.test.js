var assert = require('assert')
const parseColor = require('./color')

describe('Color Parser', function () {
    it('Colors with #', function () {
        assert.deepEqual(parseColor("#0f1f2f"), { r: 15, g: 31, b: 47 })
    })
    it('Colors without #', function () {
        assert.deepEqual(parseColor("0f1f2f"), { r: 15, g: 31, b: 47 })
    })
    it('Colors with alpha', function () {
        assert.deepEqual(parseColor("#0f1f2fff"), { r: 15, g: 31, b: 47, a: 255 })
    })
})
