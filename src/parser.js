/**
 * 
 * @param {string} dyestring 
 */
function parseDyeString(dyestring) {
    let parts = dyestring.split(";")
    if (parts.length > 1 && parts[parts.length - 1] == "")
        parts.pop()
    return [].concat(...parts.map(parse))
}

const matchRGBCMYW = /^(R|G|B|C|M|Y|W):((?:#?[0-9a-f]{6},?)+)$/i
const matchSwapMultiple = /^S:((?:#?[0-9a-f]{6},?)+)$/i
const matchAlphaSwapMultiple = /^S:((?:#?[0-9a-f]{8},?)+)$/i

/**
 * @typedef {{method: "Recolor", channel: "R"|"G"|"B"|"C"|"M"|"Y"|"W", colorSequence: string[] }} dyeActionRecolor
 * @typedef {{method: "Swap", startColor: string, resultColor: string}} dyeActionSwap
 * @typedef {{method: "SwapWithAlpha", startColor: string, resultColor: string}} dyeActionSwapWithAlpha
 * @typedef {dyeActionRecolor | dyeActionSwap | dyeActionSwapWithAlpha} dyeAction 
 */

/**
 * 
 * @param {string} dyeStringPart 
 * @returns {dyeAction}
 */
function parse(dyeStringPart) {

    if (matchRGBCMYW.test(dyeStringPart)) {
        const resultArray = matchRGBCMYW.exec(dyeStringPart)
        const colorSequence = resultArray[2].split(',')
        colorCheckArrayGuard(colorSequence)
        return {
            method: "Recolor",
            channel: resultArray[1],
            colorSequence
        }
    }

    if (matchSwapMultiple.test(dyeStringPart)) {
        const resultArray = matchSwapMultiple.exec(dyeStringPart)[1].split(',')
        if (resultArray.length % 2 != 0)
            throw new Error(
                `Dyestring Part '${dyeStringPart}' couldn't be parsed: Number of arguments isn't a multiple of 2`
                + `\n -> you probably forgot a color`
            )
        colorCheckArrayGuard(resultArray)
        let result = []
        for (let i = 0; i < resultArray.length; i = i + 2) {
            result.push({
                method: "Swap",
                startColor: resultArray[i],
                resultColor: resultArray[i + 1]
            })
        }
        return result
    }

    if (matchAlphaSwapMultiple.test(dyeStringPart)) {
        const resultArray = matchAlphaSwapMultiple.exec(dyeStringPart)[1].split(',')
        if (resultArray.length % 2 != 0)
            throw new Error(
                `Dyestring Part '${dyeStringPart}' couldn't be parsed: Number of arguments isn't a multiple of 2`
                + `\n -> you probably forgot a color`
            )
        colorCheckArrayGuard(resultArray)
        let result = []
        for (let i = 0; i < resultArray.length; i = i + 2) {
            result.push({
                method: "SwapWithAlpha",
                startColor: resultArray[i],
                resultColor: resultArray[i + 1]
            })
        }
        return result
    }

    throw new Error(`Dyestring Part '${dyeStringPart}' couldn't be parsed: Malformed or Unknown Format`)
}

const colorCheckRegExp = /^#?[0-9a-f]{6,8}$/i

function colorCheckArrayGuard(array){
    for (let i = 0; i < array.length; i++) {
        if(!colorCheckRegExp.test(array[i]))
            throw new Error()
    }
}

module.exports = parseDyeString